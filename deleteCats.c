/////////////////////////////////////////////////////////////////////////////
//////////
/////////// University of Hawaii, College of Engineering
/////////// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///////////
/////////// @file deleteCats.c
/////////// @version 1.0
///////////
/////////// @author Brayden Suzuki <braydens@hawaii.edu>
/////////// @date 21_Feb_2022
/////////////////////////////////////////////////////////////////////////////////////
#include <stdio.h>
#include <stdlib.h>
#include "catDatabase.h"

void deleteAllCats() {
   for (int i = 0; i < MAX_CATS; i++) {
      for (int j = 0; j < MAX_CAT_NAMES; j++) {
         catDatabase[i].name[j] = 0;
      }
      catDatabase[i].gender = 0;
      catDatabase[i].breed = 0;
      catDatabase[i].isfixed = 0;
      catDatabase[i].weight = 0;
      catDatabase[i].collarColor1 = 0;
      catDatabase[i].collarColor2 = 0;
      catDatabase[i].license = 0;
   }
}

void deleteCat(unsigned long index) {
   for (int j = 0; j < MAX_CAT_NAMES; j++) {
      catDatabase[index].name[j] = 0;
   }
   catDatabase[index].gender = 0;
   catDatabase[index].breed = 0;
   catDatabase[index].isfixed = 0;
   catDatabase[index].weight = 0;
   catDatabase[index].collarColor1 = 0;
   catDatabase[index].collarColor2 = 0;
   catDatabase[index].license = 0;
}

