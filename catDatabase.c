///////////////////////////////////////////////////////////////////////////////
///// University of Hawaii, College of Engineering
///// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
/////
///// @file catDatabase.c
///// @version 1.0
/////
///// @author Brayden Suzuki <braydens@hawaii.edu>
///// @date 21_Feb_2022
/////////////////////////////////////////////////////////////////////////////////
#include "catDatabase.h"

unsigned long NUM_CATS = 0;

struct Cats catDatabase[MAX_CATS];

char* getGender(enum Gender gender) {
   switch(gender) {
      case 0:
         return "UNKOWN_GENDER" ;
      case 1:
         return "MALE" ;
      case 2:
         return "FEMALE" ;
   }
   return "UNKOWN_GENDER" ;
}

char* getBreed(enum Breed breed) {
   switch(breed) {
      case 0:
         return "UNKNOWN_BREED" ;
      case 1:
         return "MAINE_COON" ;
      case 2:
         return "MANX" ;
      case 3:
         return "SHORTHAIR" ;
      case 4:
         return "PERSIAN" ;
      case 5:
         return "SPHYNX" ;
   }
   return "UNKNOWN_BREED" ;
}

char* getCollarColor(enum Color color) {
   switch(color) {
      case 0:
         return "UNKNOWN_COLOR" ;
      case 1:
         return "BLACK" ;
      case 2:
         return "WHITE" ;
      case 3: 
         return "RED" ;
      case 4:
         return "YELLOW" ;
      case 5:
         return "ORANGE" ;
      case 6: 
         return "GREEN" ;
      case 7:
         return "BLUE" ;
      case 8:
         return "PURPLE" ;
      case 9:
         return "PINK" ;
   }
   return "UNKNOWN_COLOR" ;
}

