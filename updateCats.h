/////////////////////////////////////////////////////////////////////////////
//////
/////// University of Hawaii, College of Engineering
/////// @brief Lab 07d - Animal Farm 1 - EE 205 - Spr 2022
///////
/////// @file updateCats.h
/////// @version 1.0
///////
/////// @author Brayden Suzuki <braydens@hawaii.edu>
/////// @date 21_Feb_2022
/////////////////////////////////////////////////////////////////////////////////

extern void updateCatName(unsigned long catIndex, char new_name[]);
extern void fixCat(unsigned long catIndex);
extern void updateCatWeight(unsigned long catIndex, float new_weight);
extern void updateCatCollar1(unsigned long catIndex, enum Color newCollarColor1 );
extern void updateCatCollar2(unsigned long catIndex, enum Color newCollarColor2 );
extern void updateLicense(unsigned long catIndex, unsigned long long newLicense );
